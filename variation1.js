$( document ).ready(function() {
//new buttons
        var s= $('<input type="button" class="btn0" id="stronger" value="Get Stronger"/>');
        var l= $('<input type="button" class="btn0" id="learner"value="Get Learner"/>');
        var f= $('<input type="button" class="btn0" id="faster" value="Get Faster"/>');

        $('.tabbed-overview__copy').after(f);
        $('.tabbed-overview__copy').after(l);
        $('.tabbed-overview__copy').after(s);

// adding class to filter the cattegory

//stronger
$(".class-list__item:nth-child(1)").addClass("stronger");
$(".class-list__item:nth-child(4)").addClass("stronger");
$(".class-list__item:nth-child(13)").addClass("stronger");

//learner
$(".class-list__item:nth-child(2)").addClass("learner");
$(".class-list__item:nth-child(3)").addClass("learner");
$(".class-list__item:nth-child(5)").addClass("learner");
$(".class-list__item:nth-child(6)").addClass("learner");
$(".class-list__item:nth-child(7)").addClass("learner");
$(".class-list__item:nth-child(8)").addClass("learner");
$(".class-list__item:nth-child(9)").addClass("learner");
$(".class-list__item:nth-child(10)").addClass("learner");
$(".class-list__item:nth-child(11)").addClass("learner");

//faster
$(".class-list__item:nth-child(12)").addClass("faster");
$(".class-list__item:nth-child(14)").addClass("faster");
$(".class-list__item:nth-child(15)").addClass("faster");
$(".class-list__item:nth-child(16)").addClass("faster");
$(".class-list__item:nth-child(17)").addClass("faster");
$(".class-list__item:nth-child(18)").addClass("faster");
$(".class-list__item:nth-child(19)").addClass("faster");
$(".class-list__item:nth-child(20)").addClass("faster");

$("ul.class-list__list").attr("id","categories");

//modifying text from "All les mills workouts" to "what is your fitness goal"
$('.tabbed-overview__title').text('What is your fitness goal?');
$('.tabbed-overview__copy').text('From flexibility to HIIT and everything inbetween. choose type exercise you enjoy and we can then personalise our program suggestion to suit your needs.');

//selection of 3 new buttons (fitness goals list) in each category
var $but = $('.btn0').click(function() {
    var $categ = $('.'+ this.id).show();
    $('#categories > li').not($categ).hide();

      $but.removeClass('active');
      $(this).addClass('active');
})

//editting button's attribute
$('.btn0').css({ "background-color": "#1dc99d","color":"#fff","text-transform":"uppercase","border-radius":"5px","border": "none","padding":"17px 51px","font-size": "13px","font-weight":"bold","margin": "30px 41px ","font-family": "arial"});
});

